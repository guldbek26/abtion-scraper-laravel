<?php
// API
/*
/api/{type}                  Henter alt data fra afgange / ankomste.
/api/{type}/{city}           Henter alle ankomste fra en by / Henter alle afgange fra en by
/api/{type}/{date}           Henter alle ankomste fra en by
*/

Route::get('/api/{type}', 'ScrapeController@fetch');
Route::get('/api/{type}/time/{time}', 'ScrapeController@fetchTime');
Route::get('/api/{type}/city/{city}/time/{time}', 'ScrapeController@fetchCity');
Route::get('/api/{type}/date/{date}/time/{time}', 'ScrapeController@fetchDate');
Route::get('/api/{type}/city/{city}/date/{date}/time/{time}', 'ScrapeController@fetchCityDate');



Route::get('/', function() {
  return view('welcome');
});
